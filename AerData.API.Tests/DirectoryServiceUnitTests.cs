﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions.TestingHelpers;
using System.Threading.Tasks;
using AerData.API.Services.Implementation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AerData.API.Tests
{
    [TestClass]
    public class DirectoryServiceUnitTests
    {
        /// <summary>
        /// Note: this files are used for demonstration purpose and are not to be distributed.
        /// </summary>
        private readonly Dictionary<string, string> _files = new Dictionary<string, string>
        {
            {"Alice_s_Adventures_In_Wonderland.pdf", @"D:\Demo1\file1.pdf"},
            {"Lord_of_the_Rings_Collection.pdf", @"D:\Demo2\file2.pdf"},
            {"The Education of a Bodybuilder (OCR) by Arnold Schwarzenegger.pdf",@"D:\Demo1\file3.pdf"},
            {"The Monk Who Sold His Ferrari.pdf", @"D:\Demo3\file4.pdf"}
        };


        [TestMethod]
        public async Task DirectoryService_GetDirectoriesPositiveTest()
        {
            // Arrange
            var fileSystem = GetMockedFileSystem();
            var controller = new DirectoryService(fileSystem);

            //Act
            var expected = await controller.GetDirectories(@"D:\", 3);

            //Assert
            Assert.IsNotNull(expected);
            Assert.AreEqual(expected[0].FolderName, "Demo2");
            Assert.AreEqual(expected[1].FolderName, "Demo1");
            Assert.AreEqual(expected[2].FolderName, "Demo3");
        }


        private MockFileSystem GetMockedFileSystem()
        {
            var testFileFolder = System.Environment.CurrentDirectory.Replace(@"\bin\Debug", @"\TestFiles\");
            var mockedFiles = new Dictionary<string, MockFileData>();

            foreach (var file in _files)
            {
                var fileName = $"{testFileFolder}{file.Key}";
                FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(fileName).Length;
                byte[] buff = br.ReadBytes((int)numBytes);

                mockedFiles.Add(file.Value, new MockFileData(buff));

            }
            return new MockFileSystem(mockedFiles); ;
        }

 
    }
}
