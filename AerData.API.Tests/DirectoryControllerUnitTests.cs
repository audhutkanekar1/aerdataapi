﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using AerData.API.Controllers;
using AerData.API.DTOs.Request;
using AerData.API.DTOs.Response;
using AerData.API.Services.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AerData.API.Tests
{
    [TestClass]
    public class DirectoryControllerUnitTests
    {

        #region Positive tests

        /// <summary>
        /// Positive test for the Directory Controller
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DirectoryController_GetTopDirectoriesPositiveTest()
        {
            //Arrange
            var input = new DirectoryRequest { DirectoryPath = "F:\\MyDocs", TopCount = 5 };

            var directoryServiceMock = new Mock<IDirectoryService>();
            directoryServiceMock.Setup(service => service.GetDirectories(input.DirectoryPath, input.TopCount)).
                Returns(Task.FromResult(GetTopDirectoriesPositiveTestData()));

            var controller = new DirectoryController(directoryServiceMock.Object);
            
            //Act
            var expected = await controller.GetTopDirectories(input) as OkNegotiatedContentResult<List<DirectoryEntity>>;

            //Assert
            Assert.IsNotNull(expected);
            Assert.AreEqual(expected.Content.Count, 3);

            Assert.AreEqual(expected.Content[0].FolderName, "ABC");
            Assert.AreEqual(expected.Content[0].Size, 160);

            Assert.AreEqual(expected.Content[1].FolderName, "EFG");
            Assert.AreEqual(expected.Content[1].Size, 60);

            Assert.AreEqual(expected.Content[2].FolderName, "PQR");
            Assert.AreEqual(expected.Content[2].Size, 30);

        }

        private List<DirectoryEntity> GetTopDirectoriesPositiveTestData()
        {
            var output = new List<DirectoryEntity>
            {
                new DirectoryEntity {FolderName = "ABC", Size = 160, SizeUnit = "MB"},
                new DirectoryEntity {FolderName = "EFG", Size = 60, SizeUnit = "MB"},
                new DirectoryEntity {FolderName = "PQR", Size = 30, SizeUnit = "MB"}
            };
            return output;
        }
        #endregion


        #region Negative tests

        /// <summary>
        ///  Validation test for the Directory Controller
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task DirectoryController_GetTopDirectoriesModelStateTest()
        {

            //Arrange
            var input = new DirectoryRequest { DirectoryPath = "C:\\", TopCount = 5 };

            var directoryServiceMock = new Mock<IDirectoryService>();
            directoryServiceMock.Setup(service => service.GetDirectories(input.DirectoryPath, input.TopCount)).Returns(Task.FromResult(GetTopDirectoriesPositiveTestData()));

            var controller = new DirectoryController(directoryServiceMock.Object);
            controller.ModelState.AddModelError("DirectoryPath", $"{input.DirectoryPath} not allowed.");

            //Act
            var expected = await controller.GetTopDirectories(input);

            Assert.IsInstanceOfType(expected, typeof(InvalidModelStateResult));
            var modelState = ((InvalidModelStateResult)expected).ModelState;

            //Assert
            Assert.IsTrue(modelState.Keys.Contains("DirectoryPath"));
            Assert.IsTrue(modelState["DirectoryPath"].Errors.Count == 1);

        }

        #endregion

    }
}
