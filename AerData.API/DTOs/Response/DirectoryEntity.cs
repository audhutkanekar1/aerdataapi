﻿namespace AerData.API.DTOs.Response
{
    public class DirectoryEntity
    {
        public string FolderName { get; set; }
        public double Size { get; set; }
        public string SizeUnit { get; set; }

    }
}