﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AerData.API.CustomValidators;

namespace AerData.API.DTOs.Request
{
    public class DirectoryRequest
    {
        [Required]
        [DirectoryPathValidator]
        public string DirectoryPath { get; set; }

        [Required]
        public int TopCount { get; set; }
    }
}