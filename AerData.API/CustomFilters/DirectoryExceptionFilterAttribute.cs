﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace AerData.API.CustomFilters
{
    public class DirectoryExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            //context.Exception log it
            var res = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            // res.Content = new StringContent( context.Exception.Message);
            res.Content = new StringContent("An error occurred on the server! Please contact the api provider for more details.");
            context.Response = res;


        }

        // public virtual void OnException(HttpActionExecutedContext actionExecutedContext);
        // public virtual Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken);

    }
}