﻿using System.IO;
using AerData.API.DIResolver;
using AerData.API.Services.Interface;
using System.IO.Abstractions;
using System.Web.Http;
using System.Web.Http.Cors;
using AerData.API.Services.Implementation;
using Serilog;
using Unity;
using WebApiThrottle;

namespace AerData.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            //CORS
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "*");
            config.EnableCors(cors);


            //Set throttling limit
            config.MessageHandlers.Add(new ThrottlingHandler()
            {
                Policy = new ThrottlePolicy(perSecond: 4, perMinute: 60)
                {
                    IpThrottling = true
                },
                Repository = new CacheRepository()
            });


            // Unity configuration
            var container = new UnityContainer();
            container.RegisterType<IDirectoryService, DirectoryService>();
            container.RegisterType<IFileSystem, FileSystem>();
            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

          
        }
    }
}
