﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AerData.API.CustomFilters;
using AerData.API.DTOs.Request;
using AerData.API.Services.Implementation;
using AerData.API.Services.Interface;

namespace AerData.API.Controllers
{
    // [RoutePrefix("api/v1/Directory")]
    [DirectoryExceptionFilter]
    public class DirectoryController : ApiController
    {

        private readonly IDirectoryService _directoryService;

        public DirectoryController(IDirectoryService directoryService)
        {
            _directoryService = directoryService;
        }

        /* Not going ahead with a GET api  due to the query string length constraints*/
        //[HttpGet]
        //public async Task<IHttpActionResult> GetTopFive(string rootPath)
        //{
        //    var directoryList = await _directoryService.GetDirectories(rootPath, 5);
        //    return Ok(directoryList);
        //}


        /// <summary>
        /// API to get the Top directories,the Top count and the root folder are the input to this API
        /// </summary>
        /// <param name="directoryRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IHttpActionResult> GetTopDirectories(DirectoryRequest directoryRequest)
        {
            if (ModelState.IsValid)
            {
                var directoryList = await _directoryService.GetDirectories(directoryRequest.DirectoryPath, directoryRequest.TopCount);
                return Ok(directoryList);
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

    }
}
