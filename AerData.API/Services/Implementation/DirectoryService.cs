﻿using AerData.API.DTOs.Response;
using AerData.API.Services.Interface;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Threading.Tasks;

namespace AerData.API.Services.Implementation
{
    public class DirectoryService : IDirectoryService
    {
        private readonly IFileSystem _directorySystem;

        public DirectoryService(IFileSystem directorySystem)
        {
            _directorySystem = directorySystem;
        }
        
        /// <summary>
        /// Gets the list of the directories 
        /// </summary>
        /// <param name="rootDirectory"></param>
        /// <param name="topCount"></param>
        /// <returns></returns>
        public async Task<List<DirectoryEntity>> GetDirectories(string rootDirectory, int topCount)
        {
            var contents = _directorySystem.DirectoryInfo.FromDirectoryName(rootDirectory).GetDirectories().
                Where(d => !(d.Attributes.HasFlag(FileAttributes.Hidden) || d.Attributes.HasFlag(FileAttributes.System)));

            List<Task<DirectoryEntity>> taskResults = new List<Task<DirectoryEntity>>();

            foreach (var currentFolder in contents)
            {
                taskResults.Add(Task.Run(() => GetDirectoryInfo(currentFolder)));
            }
            
            var directoryResult = await Task.WhenAll(taskResults);
            return directoryResult.OrderByDescending(d => d.Size).Take(topCount).ToList();
        }


        DirectoryEntity GetDirectoryInfo(DirectoryInfoBase parentDirectory)
        {
            //Thread.Sleep(1000);
            return new DirectoryEntity { FolderName = parentDirectory.Name, Size = GetDirectorySize(parentDirectory.FullName), SizeUnit = "MB" };
        }
        
        double GetDirectorySize(string parentDirectory)
        {
            var length = _directorySystem.DirectoryInfo.FromDirectoryName(parentDirectory).GetFiles("*.*", SearchOption.AllDirectories).Sum(file => file.Length);
            return ConvertBytesToMegabytes(length);
        }

        /// <summary>
        /// converts bytes to  megabytes
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        double ConvertBytesToMegabytes(long bytes)
        {
            if (bytes != 0)
                return Math.Round((bytes / 1024f) / 1024f, 2);
            else
                return 0;
        }
    }
}