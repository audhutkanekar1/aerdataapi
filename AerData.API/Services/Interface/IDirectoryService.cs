﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AerData.API.DTOs.Response;

namespace AerData.API.Services.Interface
{
    public interface IDirectoryService
    {
         Task<List<DirectoryEntity>> GetDirectories(string rootDirectory, int topCount);
    }
}
