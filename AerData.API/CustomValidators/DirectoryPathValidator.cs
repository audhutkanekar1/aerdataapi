﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace AerData.API.CustomValidators
{
    public class DirectoryPathValidator : ValidationAttribute
    {
        //add new exclusions to meet business/security rules
        protected string[] Exclusions = new string[] { "C:\\" };

        protected override ValidationResult IsValid(object directoryPath, ValidationContext validationContext)
        {
            var result = IsPathValid(directoryPath.ToString());
            if (result.isSuccess)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(result.validationMessage);
            }
        }

        protected (bool isSuccess, string validationMessage) IsPathValid(string path)
        {
            if (Directory.Exists(path))
            {
                //put more conditions to exclude specific paths or specify in exclusions to satisfy business/security rules
                if (Exclusions.Count(x => x.Equals(path)) > 0)
                {
                    return (false, $"{path} not allowed.");
                }

                return (true, "");
            }
            else
                return (false, "Invalid Path");

        }

    }
}